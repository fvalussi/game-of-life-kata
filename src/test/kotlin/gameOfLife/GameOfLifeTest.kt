package gameOfLife

import org.junit.Assert.assertFalse
import org.junit.Assert.assertTrue
import org.junit.Test

class GameOfLifeTest {

    @Test
    fun `Any live cell with fewer than two live neighbours dies, as if caused by underpopulation`() {
        val game = `given a game`()
        val cellToDie = `given a cell is added isolated`(game)
        val locationOfCellToDie = cellToDie.location

        game.nextGeneration()

        assertFalse(game.hasCellIn(locationOfCellToDie))
    }

    @Test
    fun `Any live cell with more than three live neighbours dies, as if by overcrowding`() {
        val game = `given a game`()
        `given four cells are added together`(game)
        val cellToDie = `given a cell is added next to the others`(game)
        val locationOfCellToDie = cellToDie.location

        game.nextGeneration()

        assertFalse(game.hasCellIn(locationOfCellToDie))
    }

    @Test
    fun `Any live cell with three live neighbours lives on to the next generation`() {
        val game = `given a game`()
        `given three cells are added together`(game)
        val cellToSurvive = `given a cell is added next to the others`(game)
        val cellToSurviveLocation = cellToSurvive.location

        game.nextGeneration()

        assertTrue(game.hasCellIn(cellToSurviveLocation))
    }

    @Test
    fun `Any live cell with two live neighbours lives on to the next generation`() {
        val game = `given a game`()
        `given two cells are added together`(game)
        val cellToSurvive = `given a cell is added next to the others`(game)
        val cellToSurviveLocation = cellToSurvive.location

        game.nextGeneration()

        assertTrue(game.hasCellIn(cellToSurviveLocation))
    }

    private fun `given a game`(): GameOfLife {
        return GameOfLife()
    }

    private fun `given four cells are added together`(game: GameOfLife) {
        game.addCell(Cell(Location(1, 1)))
        game.addCell(Cell(Location(1, 2)))
        game.addCell(Cell(Location(1, 3)))
        game.addCell(Cell(Location(1, 2)))
    }

    private fun `given three cells are added together`(game: GameOfLife) {
        game.addCell(Cell(Location(2, 1)))
        game.addCell(Cell(Location(1, 2)))
        game.addCell(Cell(Location(3, 2)))
    }

    private fun `given two cells are added together`(game: GameOfLife) {
        game.addCell(Cell(Location(2, 1)))
        game.addCell(Cell(Location(1, 2)))
    }

    private fun `given a cell is added next to the others`(game: GameOfLife): Cell {
        val cell = Cell(Location(2, 2))
        game.addCell(cell)

        return cell
    }

    private fun `given a cell is added isolated`(game: GameOfLife): Cell {
        val cell = Cell(Location(99, 99))
        game.addCell(cell)

        return cell
    }
}

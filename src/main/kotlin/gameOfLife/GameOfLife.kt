package gameOfLife

import kotlin.math.pow
import kotlin.math.sqrt

class GameOfLife {
    private var cells = mutableListOf<Cell>()
    private var neighborCounter = 0

    fun addCell(cell: Cell) {
        cells.add(cell)
    }

    fun nextGeneration() {
        val survivingCells = mutableListOf<Cell>()
        val auxCells = cells
        cells.forEach { checkedCell ->
            this.neighborCounter = 0
            auxCells.forEach { auxCell ->
                areNeighbors(checkedCell, auxCell)
            }
            cellSurvivesIfItHasTwoNeighbors(survivingCells, checkedCell)
        }

        cells = survivingCells
    }

    private fun cellSurvivesIfItHasTwoNeighbors(
        survivingCells: MutableList<Cell>,
        checkedCell: Cell
    ) {
        if (this.neighborCounter == 2 || this.neighborCounter == 3) {
            survivingCells.add(checkedCell)
        }
    }

    private fun areNeighbors(checkedCell: Cell, auxCell: Cell) {
        val distance = getDistanceBetween(checkedCell.location, auxCell.location)
        if (distance > 0.0 && distance < 2.0) {
            this.neighborCounter++
        }
    }

    private fun getDistanceBetween(location1: Location, location2: Location): Double {
        val x1 = location1.x
        val y1 = location1.y
        val x2 = location2.x
        val y2 = location2.y

        return sqrt((x2 - x1).toDouble().pow(2.0) + (y2 - y1).toDouble().pow(2.0) * 1.0)
    }

    fun hasCellIn(location: Location): Boolean {
        cells.forEach { cell ->
            if (cell.location == location) {
                return true
            }
        }

        return false
    }
}
